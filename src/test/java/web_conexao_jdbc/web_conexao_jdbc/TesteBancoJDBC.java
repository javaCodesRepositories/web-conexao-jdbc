package web_conexao_jdbc.web_conexao_jdbc;

import java.util.List;

import org.junit.Test;

import dao.TelefoneDAO;
import dao.UserDAO;
import model.BeanUserFoneInnerJoin;
import model.TelefoneUser;
import model.User;

public class TesteBancoJDBC {

	@Test
	public void initInsertBanco() {
		User user = new User();
		user.setNome("Rosinele Lucas");
		user.setEmail("rosinelelucas@gmail.com");
		
		new UserDAO().salvar(user);
		
	}
	
	@Test
	public void initSelectBanco() {
		
		try {
			List<User> listaUsers= new UserDAO().listarUsuarios();
			
			for (User user : listaUsers) {
				System.out.println(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void initBuscaUsuarioBanco() {
		
		try {
			
			User u = new UserDAO().pesquisaUser(6L);
			System.out.println(u);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initUpdateUsuarioBanco() {
		
		
		try {
		
		User user = new UserDAO().pesquisaUser(2L);
		user.setNome("Augusto Dutra");
		user.setEmail("augustodutra16@gmail.com");
		
			new UserDAO().updateUser(user);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initdeleteUser() {
		
		try {
			
			new UserDAO().deleteUser(8L);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/*-------------------------------------DIVISAO DE TESTES-------------------------------------------------------------*/
	
	@Test
	public void initInsertTelefoneUser() {
		
		try {
			
			User user = new UserDAO().pesquisaUser(9L);
			
			TelefoneUser telefoneUser = new TelefoneUser();
			telefoneUser.setUser(user);
			telefoneUser.setNumero("(21)99541-1321");
			telefoneUser.setTipo("celular");
			
			new TelefoneDAO().insertTelefone(telefoneUser);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test public void initSelectALLTelefone() {
		
		try {
			
			List<TelefoneUser> listaTelefoneUsers = new TelefoneDAO().listarTelefones();
			
			for (TelefoneUser telefoneUser : listaTelefoneUsers) {
				System.out.println(telefoneUser);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void initBuscaTelefone() {
		
		try {
			
			TelefoneUser telefoneUser = new TelefoneDAO().buscaTelefoneUser(5L);
			
			System.out.println(telefoneUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initUpdateTelefone() {
		
		try {
			
			TelefoneUser telefoneUser = new TelefoneDAO().buscaTelefoneUser(9L);
			telefoneUser.setNumero("(21)99780-6645");
			telefoneUser.setTipo("celular");
			
			new TelefoneDAO().updateTelefoneUser(telefoneUser);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void initdeletarTelefone() {
		
		try {
			
			new TelefoneDAO().deleteTelefone(7L);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void initListarInnerJoin() {
		
		try {
			
			List<BeanUserFoneInnerJoin> list = new UserDAO().listaInnerJoinUserFone(2L);
			
			for (BeanUserFoneInnerJoin beanUserFoneInnerJoin : list) {
				System.out.println(beanUserFoneInnerJoin);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void initDeleteTelefoneUserRelacional() {
		
		new UserDAO().deleteTelefoneUserRelacional(4L);
		
	}
	
}
